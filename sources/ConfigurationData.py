﻿import bitstruct
from functools import reduce

def mac_from_string(s):
    i = reduce(lambda a,b: a<<8 | b, map( lambda si: int(si if si else '0',  16) , s.split(":")))
    return i

def mac_to_string(i):
    s = ":".join(map(lambda n: format((i>>n & 0xFF),  '02x'), [40, 32, 24,16,8,0]))
    return s

def ip_from_string(s):
    "Convert dotted IPv4 address to integer."
    return reduce(lambda a,b: a<<8 | b, map(lambda si: int(si if si else '0',  10), s.split(".")))

def ip_to_string(ip):
    "Convert 32-bit integer to dotted IPv4 address."
    return ".".join(map(lambda n: str(ip>>n & 0xFF), [24,16,8,0]))

fields = [
    {'name': 'reserved',               'pos': 0,    'len': 4*4*8 ,  'type': int,    'ptype':'u'},
    {'name': 'MAC_ADDR',               'pos': 128,  'len': 6*8,     'type': int,    'ptype':'u',  'to_string':mac_to_string,  'from_string':mac_from_string},
    {'name': 'VLAN_ID',                'pos': 176,  'len': 16,      'type': int,    'ptype':'u',  'format':'03x'},
    {'name': 'Reserved2',              'pos': 192,  'len': 128,     'type': int,    'ptype':'u'},
    {'name': 'ARP_CLIENT_TIMEOUT_MS',  'pos': 288,  'len': 16,      'type': int,    'ptype':'u'},
    {'name': 'Reserved3',              'pos': 304,  'len': 16,      'type': int,    'ptype':'u'},
    {'name': 'ARP_SERVER_CTRL',        'pos': 320,  'len': 8,       'type': int,    'ptype':'u'},
    {'name': 'Reserved4',              'pos': 316,  'len': 24,      'type': int,    'ptype':'u'},
    {'name': 'IPv4_ADDR',              'pos': 340,  'len': 32,      'type': int,    'ptype':'u',  'to_string':ip_to_string,  'from_string':ip_from_string},
    {'name': 'IPv4_MASK',              'pos': 372,  'len': 32,      'type': int,    'ptype':'u',  'to_string':ip_to_string,  'from_string':ip_from_string},
    {'name': 'IPv4_GATEWAY',           'pos': 404,  'len': 32,      'type': int,    'ptype':'u',  'to_string':ip_to_string,  'from_string':ip_from_string},
    {'name': 'Reserved5',              'pos': 348,  'len': 32*5,    'type': int,    'ptype':'u'},
    {'name': 'ICMP_DISABLE',           'pos': 508,  'len': 8,       'type': int,    'ptype':'u'},
    {'name': 'Reserved4',              'pos': 516,  'len': 24,      'type': int,    'ptype':'u'},
    {'name': 'IGMP_DISABLE',           'pos': 532,  'len': 8,       'type': int,    'ptype':'u'},
    {'name': 'Reserved4',              'pos': 540,  'len': 24,      'type': int,    'ptype':'u'},
]
dataSize = sum([d['len'] for d in fields])//8
unpackStr = str()
bytesSwapStr = []
for field in fields:
    unpackStr += field['ptype']+ str(field['len'])
    bytesSwapStr.append( int(field['len'])//8 )
        
def getField(key):
    l = [item for item in fields if item["name"] == key]
    return l[0] if len(l) else None

def pack_into(data):
    values = []
    for n in [f['name'] for f in fields]:
        if n in data: values.append(data[n])
        else: values.append(0)
    ba = bitstruct.pack(unpackStr,  *values)
    assert(len(ba) == dataSize)
    return bitstruct.byteswap(bytesSwapStr, ba)
    
def unpack_from(ba):
    assert(len(ba) == dataSize)
    values = bitstruct.unpack(unpackStr,  bitstruct.byteswap(bytesSwapStr, ba[0:dataSize]))
    data = dict(zip([f['name'] for f in fields], values))
    return data
