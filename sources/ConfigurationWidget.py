﻿from parse import *
from PyQt5.QtWidgets import QWidget
import ConfigurationData
from PyQt5.uic import loadUi

class ConfigurationWidget(QWidget):
    field_prefix = 'field_'
    
    def __init__(self, parent = None):
        super(QWidget, self).__init__(parent)
        loadUi('ConfigurationWidget.ui', self)
        
    def setWidgetValue(self,  widget,  value,  field):
        if widget.inherits("QLineEdit"):
            if field:
                if'format' in field and field['format'] is not None:
                    widget.setText(format(value, field['format']))
                elif 'to_string' in field and field['to_string'] is not None:
                    widget.setText(field['to_string'](value))
            elif isinstance(value,  str): widget.setText(value)
            elif isinstance(value,  int): widget.setText(str(hex(value)[2:] ))
        elif widget.inherits("QCheckBox"): widget.setChecked(value)
        elif widget.inherits("QSpinBox"): widget.setValue(int(value))

    def getWidgetValue(self,  widget,  field):
        if widget.inherits("QLineEdit"): 
            text =  widget.text()
            if field:
                if'format' in field and field['format'] is not None:
                    text = widget.text().translate({ord(i):None for i in widget.inputMask()})
                    if 0 == len(text): return field['type']()
                    return parse('{:' + field['format'] + '}', text)[0]
                elif 'to_string' in field and field['to_string'] is not None:
                    return field['from_string'](text)
            return field['type'](text) if len(text) else 0
        elif widget.inherits("QCheckBox"): return widget.isChecked()
        elif widget.inherits("QSpinBox"): return widget.value()

    def getConfigurationData(self):
        result = dict()
        for widget in self.children():
            if widget.objectName().startswith(self.field_prefix):
                key = widget.objectName()[len(self.field_prefix):]
                result[key] = self.getWidgetValue(widget,  ConfigurationData.getField(key))
        return result
        
    def setConfigurationData(self, data):
        for key,  value in data.items():
            widget = self.findChild(QWidget,  self.field_prefix + key)
            if widget:
                self.setWidgetValue(widget,  value,  ConfigurationData.getField(key))
