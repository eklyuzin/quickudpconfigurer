﻿from PyQt5.QtCore import pyqtSlot, QSettings, QByteArray
from PyQt5.QtWidgets import QApplication, QDialog, QFileDialog, QMessageBox
import ConfigurationWidget
import ConfigurationData
from PyQt5.uic import loadUi
import os.path
import logging
import sys

setting_Geometry = 'WindowGeometry'
setting_FileName = 'FileLocation'
ITEMS_NUM = 6

logging.basicConfig(stream=sys.stderr, level=logging.INFO)

class Dialog(QDialog): 
    def __init__(self, parent = None):
        super(Dialog, self).__init__(parent)
        loadUi('maindialog.ui', self)
        self.configurationWidget = ConfigurationWidget.ConfigurationWidget(self.frameConfigurationWidget);
        self.frameConfigurationWidgetLayout.addWidget(self.configurationWidget)
        
        self.devicesSelecter.buttonClicked[int].connect(self.on_deviceSelecter_clicked)
        self.settings = QSettings('Quick UDP Configurer', 'Katorga')
        self.restoreGeometry(self.settings.value(setting_Geometry, QByteArray()))
        self.lineEditFileLocation.setText(self.settings.value(setting_FileName,  ''))
        
        self.configurationDataList = []
        for i in range(ITEMS_NUM):
            item = {
                'data' : {}
                }
            self.configurationDataList.append(item)
        self.devicesSelecter.setId(self.radioButtonDevice0, 0)
        self.devicesSelecter.setId(self.radioButtonDevice1, 1)
        self.devicesSelecter.setId(self.radioButtonDevice2, 2)
        self.devicesSelecter.setId(self.radioButtonDevice3, 3)
        self.devicesSelecter.setId(self.radioButtonDevice4, 4)
        self.devicesSelecter.setId(self.radioButtonDevice5, 5)
        self.previousId = 0
        self.radioButtonDevice0.setChecked(True)

    def closeEvent(self, e):
        self.settings.setValue(setting_FileName,  self.lineEditFileLocation.text())
        self.settings.setValue(setting_Geometry, self.saveGeometry())

    @pyqtSlot() 
    def on_pushButtonBrowse_released(self):
        fname = QFileDialog.getOpenFileName(self, 'Open file')
        if fname is not None:
            self.lineEditFileLocation.setText(fname[0])
            
    @pyqtSlot() 
    def on_pushButtonLoad_released(self):
        if not os.path.isfile(self.lineEditFileLocation.text()):
            QMessageBox.warning(self, "Warning", 'File not exist: ' + self.lineEditFileLocation.text())
            return

        f = open(self.lineEditFileLocation.text(), 'rb')
        ba = bytearray(f.read())
        dataSize = ConfigurationData.dataSize
        if 0 != (len(ba) % dataSize):
            QMessageBox.warning(self, "Warning", 'Unexpected file length: ' + self.lineEditFileLocation.text())
            return
        assert((len(ba) % dataSize) == 0)
        items = min(len(ba)//dataSize,  ITEMS_NUM)
        for i in range(items):
            data = ConfigurationData.unpack_from(ba[i*dataSize: i*dataSize + dataSize])
            logging.info('Data for device # %d : %s', i, str(data))
            self.configurationDataList[i] = data
        self.loadDataToWidget()

    @pyqtSlot() 
    def on_pushButtonSave_released(self):
        self.saveWidgetToData()
        f = open(self.lineEditFileLocation.text(), 'wb')
        for i in range(ITEMS_NUM):
            data = self.configurationDataList[i]
            ba = ConfigurationData.pack_into(data);
            f.write(ba)

    def loadDataToWidget(self,  id = None):
        if (id is None): id = self.devicesSelecter.checkedId()
        data = self.configurationDataList[id]
        logging.info('Data for device # %d : %s', id, str(data))
        self.configurationWidget.setConfigurationData(data)

    def saveWidgetToData(self):
        self.configurationDataList[self.previousId] = self.configurationWidget.getConfigurationData()
        
    @pyqtSlot(int)
    def on_deviceSelecter_clicked(self,  id):
        if id < 0: return
        if (self.previousId ==id): return
        self.saveWidgetToData()
        self.previousId = id
        self.loadDataToWidget(id)
 
if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    dlg = Dialog()
    dlg.show()
    sys.exit(app.exec_()) 
